﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MSPWebPlatform.Startup))]
namespace MSPWebPlatform
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
