//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MSPEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class User
    {
        public User()
        {
            this.Laboratories = new HashSet<Laboratory>();
            this.Presences = new HashSet<Presence>();
        }
    
        public long Id { get; set; }
        public long ProfileId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    
        public virtual ICollection<Laboratory> Laboratories { get; set; }
        public virtual ICollection<Presence> Presences { get; set; }
        public virtual Profile Profile { get; set; }
    }
}
